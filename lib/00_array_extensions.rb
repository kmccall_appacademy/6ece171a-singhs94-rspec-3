# Sum
#
# Write an Array method, `sum`, that returns the sum of the elements in the
# array. You may assume that all of the elements are integers.

class Array
  def sum
    result = 0
    self.each do |x|
      result = result + x
    end
    return result
  end
end

# Square
#
# Write an array method, `square`, that returns a new array containing the
# squares of each element. You should also implement a "bang!" version of this
# method, which mutates the original array.

class Array
  def square!
    i = 0
    while i < self.length
      self[i] = self[i] * self[i]
      i = i + 1
    end
    return self

  end

  def square
    return self.collect {|x| x*x}
  end
end

# Finding Uniques
#
# Monkey-patch the Array class with your own `uniq` method, called
# `my_uniq`. The method should return the unique elements, in the order
# they first appeared:
#
# ```ruby
# [1, 2, 1, 3, 3].my_uniq # => [1, 2, 3]
# ```
#
# Do not use the built-in `uniq` method!

class Array
  def my_uniq
    unique = []
    self.each do |x|
      if unique.include?(x) == false
        unique.push(x)
      end
    end
    return unique
  end
end

# Two Sum
#
# Write a new `Array#two_sum` method that finds all pairs of positions
# where the elements at those positions sum to zero.
#
# NB: ordering matters. I want each of the pairs to be sorted smaller
# index before bigger index. I want the array of pairs to be sorted
# "dictionary-wise":
#
# ```ruby
# [-1, 0, 2, -2, 1].two_sum # => [[0, 4], [2, 3]]
# ```
#
# * `[0, 2]` before `[1, 2]` (smaller first elements come first)
# * `[0, 1]` before `[0, 2]` (then smaller second elements come first)

class Array
  def two_sum
    i = 0
    j = 1
    position_Array = []
    while i < self.length
      first = self[i]
      while j < self.length
        second = self[j]
        sum_to_Zero = first + second
        if sum_to_Zero == 0
          position_Array.push([i,j])
        end
        j = j + 1
      end
      i = i + 1
      j = i + 1
    end
    return position_Array
  end
end

# Median
#
# Write a method that finds the median of a given array of integers. If
# the array has an odd number of integers, return the middle item from the
# sorted array. If the array has an even number of integers, return the
# average of the middle two items from the sorted array.

class Array
  def median
    sorted = self.sort
    if sorted == []
      return nil
    elsif sorted.length == 1
      return sorted[0]
    elsif sorted.length%2 == 1
      return sorted[(sorted.length.to_f/2).floor]
    elsif sorted.length%2 == 0
      total = sorted[sorted.length/2] + sorted[(sorted.length/2) - 1]
      return total.to_f/2
    end

  end
end

# My Transpose
#
# To represent a *matrix*, or two-dimensional grid of numbers, we can
# write an array containing arrays which represent rows:
#
# ```ruby
# rows = [
#     [0, 1, 2],
#     [3, 4, 5],
#     [6, 7, 8]
#   ]
#
# row1 = rows[0]
# row2 = rows[1]
# row3 = rows[2]
# ```
#
# We could equivalently have stored the matrix as an array of
# columns:
#
# ```ruby
# cols = [
#     [0, 3, 6],
#     [1, 4, 7],
#     [2, 5, 8]
#   ]
# ```
#
# Write a method, `my_transpose`, which will convert between the
# row-oriented and column-oriented representations. You may assume square
# matrices for simplicity's sake. Usage will look like the following:
#
# ```ruby
# matrix = [
#   [0, 1, 2],
#   [3, 4, 5],
#   [6, 7, 8]
# ]
#
# matrix.my_transpose
#  # => [[0, 3, 6],
#  #    [1, 4, 7],
#  #    [2, 5, 8]]
# ```
#
# Don't use the built-in `transpose` method!

class Array
  #come back to this
  def my_transpose
    result = []
  	transfer = []
  	skip = []
  	for i in 0..self.length-1
  		for j in 0..self[i].length-1
  			transfer << self[i][j]
  		end
  		result << transfer
  		transfer = []
  	end
  	for i in 0..result.length-1
  		for j in 0..result[i].length-1
  			if !skip.include? i+j
	  			temp = result[j][i]
	  			result[j][i] = result[i][j]
	  			result[i][j] = temp
	  			skip << i+j
	  		end
  		end
  	end
  	result
  end
end


# Bonus: Refactor your `Array#my_transpose` method to work with any rectangular
# matrix (not necessarily a square one).
